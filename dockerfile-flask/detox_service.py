from flask import Flask, request, json
from werkzeug.utils import secure_filename
from PIL import Image
from pdf2image import convert_from_path
import pytesseract
import cv2
import os

app = Flask(__name__)

@app.route('/get-json', methods=['POST'])
def get_json():
    	f = request.files['file']
	print f.filename
    	f.save('/mnt/upload/' + secure_filename(f.filename))
	pages = convert_from_path('/mnt/upload/' + secure_filename(f.filename),dpi=500, fmt='png')
	n=0
	for page in pages:
		page.save('/mnt/upload/' + secure_filename(f.filename)+ str(n) +'.png')
		n += 1
	text = ''
	n_pr = 0
	while n_pr < n:
		#load the example image and convert it to grayscale
		image = cv2.imread('/mnt/upload/' + secure_filename(f.filename) + str(n_pr) + '.png')
		gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

		# check to see if we should apply thresholding to preprocess the
		# image
		gray = cv2.threshold(gray, 0, 255,
			cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]

		# write the grayscale image to disk as a temporary file so we can
		# apply OCR to it
		filename = "{}.png".format(os.getpid())
		cv2.imwrite(filename, gray)
		text = text + pytesseract.image_to_string(Image.open(filename),lang="spa")
		os.remove(filename)
		n_pr += 1
 
	resp = {"data":text}
	response = app.response_class(
        	response=json.dumps(resp),
	        status=200,
        	mimetype='application/json'
	    )
	os.remove('/mnt/upload/' + secure_filename(f.filename))
	return response

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
