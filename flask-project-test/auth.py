import functools
from flask import Blueprint

from db import db_session
from models import Persona

bp = Blueprint('auth', __name__, url_prefix='/auth')
@bp.route('/hello')
def hello():
	u = Persona('123456', 'Pedro Javier', 'Rivadeneira')
	db_session.add(u)
	db_session.commit()
	return 'hello'
