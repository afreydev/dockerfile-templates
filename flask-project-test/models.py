from sqlalchemy import Column, Integer, String
from db import Base

class Persona(Base):
    __tablename__ = 'persona'
    id = Column(Integer, primary_key=True)
    identificacion = Column(String(15), unique=False)
    nombres = Column(String(120), unique=False)
    apellidos = Column(String(120), unique=False)

    def __init__(self, identificacion=None,nombres=None, apellidos=None):
	self.identificacion = identificacion
        self.nombres = nombres
        self.apellidos = apellidos

    def __repr__(self):
        return '<Persona %r>' % (self.identificacion)
